Examples
========

The objective of this chapter is to provide some examples
on how to use a :class:`~mr.meeseeks.Meeseeks`.


.. toctree::
   :maxdepth: 2

   hello-world
   saving-data
