Saving Data
===========

Here is a dummy example to save data at the end of your program.
It also shows that you do not necessarily need to put filenames as 
an input.

You can see the use of :class:`~mr.meeseeks.Moment` to save data 
after all the tasks have been accomplished.

For the example, we force the number of processes to be equal to `4`.
By default, the parallelization is done automatically and the
number of processes is given by the `multiprocessing.cpu_count()` function.

.. code-block:: python
   :linenos:
   :caption: Saving data example

   #!/usr/bin/python
    
   # import the Meeseeks class and the action decorator
   from mr.meeseeks import Meeseeks, Moment, action
   # import pickle library to save data
   import pickle

   @action
   def process_project(project, data):
      "Process one project"
      data[project['name']] = project['content']

   @action(when=Moment.AFTER)
   def save_data(data):
     "Save data"
     with open('test_data.pkl', 'wb') as f:
        pickle.dump(data, f)

   if __name__ == "__main__":
      # call your Meeseeks
      projects = []
      for p in range(100):
         projects.append({
            'name': f'Project #{p}',
	    'content': f'Content here of project{p}'
         })
      m = Meeseeks(items=projects, processes=4)
