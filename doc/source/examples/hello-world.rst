Hello World
===========

Here is a basic example to start with.
For each HDF5 files under the `data` folder, 
it will print a "Hello world".

.. code-block:: python
   :linenos:
   :caption: Hello world example

   #!/usr/bin/python
    
   # import the Meeseeks class and the action decorator
   from mr.meeseeks import Meeseeks, action
    
    
   # define the action
   @action
   def hello_world(filename, _):
      print(f"Hello world from {filename}")
    
    
   if __name__ == "__main__":
      # call your Meeseeks
      m = Meeseeks(items="data/*.hdf5")
