.. Meeseeks documentation master file, created by
   sphinx-quickstart on Wed Feb 16 09:39:27 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Introduction
============

Overview
--------

This project intends to batch process many items at ones.
Where `items` are referred to a list of objects.

Meeseeks are creatures who are created to serve a singular purpose 
for which they will go to any length to fulfill.

What you'll need is:

- files or data to be processed,
- a `python` script that defines the things to be performed,
- optionnaly, a `yaml` file to describe what the `Meeseeks` must do.

We have decided to provide **two ways** of declaring your batch processes,
which can be combined.

Python
~~~~~~

The first way is to declare everything in a single `python` file,
as it will be described later on. This has the advantage of seeing the
entire process in a single file.

YAML
~~~~

To enhance the customization, you can also provide a `yaml` file to
your `Meeseeks`. This `yaml` file enables to use the functions
declared in your `python` script with different parameters and to
switch easily between those configurations.

More details will be given in the :ref:`usage` chapter.

Installation
------------

.. todo:: Provide installation instructions


.. toctree::
   :maxdepth: 2
   :hidden:
   
   self
   usage/index
   examples/index

.. toctree::
   :maxdepth: 3
   :caption: Documentation
   :hidden:

   code/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
