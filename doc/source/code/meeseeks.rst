Meeseeks module
===============

Meeseeks class
--------------

.. autoclass:: mr.meeseeks.Meeseeks
   :members:

MeeseeksWatch class
-------------------

.. autoclass:: mr.meeseeks.MeeseeksWatch
   :members:
