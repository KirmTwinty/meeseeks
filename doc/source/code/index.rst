Code Documentation
==================


The objective of this chapter is to browse the source code of
the entire module :mod:`mr`

.. toctree::
   :maxdepth: 2

   meeseeks
   scheduler
   moment
   utils
