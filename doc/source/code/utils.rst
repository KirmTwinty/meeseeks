Utils module
============

Regroup the utilities functions such as the logger and the ``Exception`` derived objects.

.. automodule:: mr.utils
   :members:

