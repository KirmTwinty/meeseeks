Scheduler module
================

The scheduler is used to register the actions and tasks, organize them 
and solve the dependencies.


Scheduler class
---------------

.. autoclass:: mr.scheduler.Scheduler
   :members:

Task class
----------

.. autoclass:: mr.scheduler.Task
   :members:


@action decorator
-----------------

.. autofunction:: mr.scheduler.action


