YAML
====

Introduction
------------

The tasks can also be defined through a ``YAML`` file, even though you need to have defined
the corresponding ``python`` functions in the same context of your :class:`~mr.meeseeks.Meeseeks`.

The following sequence diagram shows the overall process that happens when defining your ``python``
file and launching your :class:`~mr.meeseeks.Meeseeks`.

.. note::
   It is important to note that the functions that have been decorated with :func:`@action<mr.scheduler.action>`
   are **automatically** added to the scheduler. Nothing else to do.

   However, to call a function from the ``YAML`` file, it needs to be defined within your :class:`~mr.meeseeks.Meeseeks` context.
   This includes both decorated functions and not decorated ones.

.. mermaid:: yaml-workflow.mmd
   :caption: Sequence diagram presenting the overall process

YAML reference
--------------

.. code-block:: yaml
   :linenos: 
   :caption: YAML file reference

   #=================================================================
   # This is a sample YAML file for running batch processes
   # The configuration can be defined using the following tags
   # [o] stands for optionnal
   # [m] stands for mandatory

   properties:          # [o] -> properties of the overall process
      progress:         #     -> show progress of the process [true]
      log_file:         #     -> save the output to a log file
      
   input:               # [m] -> tag for defining the input data
      recursive:        # [o] -> Either to look onto subfolder or not for finding files [true]
      folders:          #     -> can be a path to a file, a folder or a list of those
      date:             # [o] -> defines the range of time to consider in the filename
         begin:         #     -> !timestamp object
         end:           #     -> !timestamp object
         format_str:    #     -> the format of the date in the filename
         regexp:        #     -> the way to extract the date from the filename
      
   tasks:               # [m] -> which task to execute in which order as a list
      - mytask
	...

   mytask:              # [m] -> The actual definition of the task "mytask"
                        #        It is associated with an action
      action:           # [m] -> The action that will be executed
                        #        If batch the input of this function must be:
	                #        (filename)
      when:             # [o] -> if not provided: will be executed for each file 
                        #        - before: will be executed in the main program before the batch actions
                        #        - after: will be executed in the main program before the batch actions
      rate:             # [o] -> file rate if on file
      beginDate:        # [o] -> Begin date if on file
      endDate:          #     -> End date if batch

.. todo:: add custom arguments to the action

.. todo:: add extra arguments to the action
.. 	  
   TODO      args:             # [o] -> arguments that will be given to the function
   TODO      extra:            # [o] -> extra arguments. For clarity, you can pass
   TODO         - name1: value1     #        arguments with the tag 'extra'. Its child tags
   TODO         - name2: value2     #        will be parsed as 'name', 'value' arguments.
	     
