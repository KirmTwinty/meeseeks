Action
======

Description
-----------

An action is a function that will be executed automatically by the :class:`~mr.meeseeks.Meeseeks` during one or more stage.
The easiest way to define an action is to use the :func:`@action<mr.scheduler.action>` `decorator <https://www.python.org/dev/peps/pep-0318/>`_.

In particular, you can execute the same function multiple times with different task parameters by using the decorator multiple times.

.. note::

   Please note the fact that on the definition of the action,
   you need to define 2 arguments: 
   1. the item you are processing
   2. a `data` dictionnary that is shared among the processes (concurrency is automatically handled)


For the impatients
------------------

Here is a quick example on how actions can be defined. For a full example, see the :ref:`Examples` one.

.. code-block:: python
   :linenos:
   :caption: Syntax for defining an action

   @action(when=Moment.BEFORE)
   def some_function(_, _):
      print("Executed before batch")

   @action
   @action(rate=2)
   @action(rate=4)
   def some_function(filename, _):
      print("Executed on each file, but also each 2 files and each 4 files")
      

Dependencies
------------

Actions are organized thanks to the :class:`~mr.scheduler.Scheduler`. Its role is to store the actions definitions and organize them.
In particular, it solves dependencies if supplied by the user.
Let suppose you need to execute the ``function_1`` after the ``function_0``. You can inform your :class:`~mr.meeseeks.Meeseeks` by using
the :attr:`depends<mr.scheduler.depends>` keyword.

.. code-block:: python
   :linenos:
   :caption: Syntax for using dependencies

   @action(depends=['function_0'])
   def function_1(filename, _):
      print("I will be executed after `function_0`")

   @action
   def function_0(filename, _):
      print("I will be executed first.")



Documentation
-------------

The following shows an extract of the documentation for the :func:`@action<mr.scheduler.action>` decorator and in particular its arguments.

.. autofunction:: mr.scheduler.action
   :noindex:
