Principle
=========

As explained in the introduction, one can call a task to be processed through two ways:

1. directly in a ``python`` file
2. additionnaly, you can define a configuration ``yaml`` file

.. note::
   The `yaml` file will be presented later on : :ref:`YAML`

How to define my batch process?
-------------------------------

1. you have to define the ``python`` functions that will be used in the batch,
2. use one or multiple decorator(s) :func:`@action<mr.scheduler.action>` on the functions to tell the :class:`~mr.meeseeks.Meeseeks`
   how and when to execute those functions,
3. call your :class:`~mr.meeseeks.Meeseeks` with information on the files to process.

   
Workflow
--------

The following figure shows the overall workflow process which is divided onto three
main stages:

1. Pre-process (``Moment.BEFORE``)
2. Process (``Moment.ON_ITEM``)
3. Post-process (``Moment.AFTER``)

.. mermaid:: workflow.mmd
   :caption: Workflow of the overall process

An action can be defined on any of those stages with the :attr:`when<mr.scheduler.when>` keyword / attribute.
