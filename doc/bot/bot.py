import os
import requests
import argparse
import logging
import re

# logging.basicConfig(
#     format='%(asctime)s @%(name)s [%(levelname)s]: %(message)s', level=os.environ.get("LOGLEVEL", "INFO"))

current_commit = os.environ.get("CI_COMMIT_SHORT_SHA")

TODO_REGEXP = r"\[([xX\s])\]\s(.*)\s\(.*\)$"

logging.basicConfig(
    format='%(asctime)s @%(name)s [%(levelname)s]: %(message)s', 
    level=logging.DEBUG)

log = logging.getLogger('documentation-bot')

parser = argparse.ArgumentParser()
parser.add_argument("base_url", help="the url of the gitlab")
parser.add_argument("project_id", help="the id of the project on the gitlab instance")
parser.add_argument("website_url", help="where is located the wbesite")
parser.add_argument("root_path", help="the root of the project")
parser.add_argument("log_path", help="path to the log file to process")
parser.add_argument("-c", "--commit", default=None, help="current commit short SHA")

args = parser.parse_args()

log.debug(f'Input arguments: {args.base_url}, {args.project_id}, {args.website_url }, {args.root_path }, {args.log_path}')

url = os.path.join(args.base_url, 'api/v4/projects/', str(args.project_id))

headers = {'PRIVATE-TOKEN': os.environ['PRIVATE_TOKEN_MEESEEKS']}


def get(path):
    try:
        response = requests.get(os.path.join(url, path), headers=headers)
        response.raise_for_status()
        # Additional code will only run if the request is successful
    except requests.exceptions.HTTPError as error:
        log.error(error)
    finally:
        return response.json()


def post(path, data):
    try:
        response = requests.post(os.path.join(url, path), 
                                 headers=headers, 
                                 data=data)
        response.raise_for_status()
        # Additional code will only run if the request is successful
    except requests.exceptions.HTTPError as error:
        log.error(error)


def update_issue(id, data):
    try:
        response = requests.put(os.path.join(url, 'issues', str(id)), 
                                 headers=headers,
                                 data=data)
        response.raise_for_status()
        # Additional code will only run if the request is successful
    except requests.exceptions.HTTPError as error:
        log.error(error)


def create_issue():
    bot_label_exists = False
    data = get('labels')
    for d in data:
        if d['name'] == 'Bot':
            bot_label_exists = True
    # Create the `Bot` label
    if not bot_label_exists:
        post('labels', data={'name': 'Bot',
                             'color': '#2BBBBD',
                             'description': 'Made automatically by a Bot.'})

    post('issues',
         data={'title': 'Documentation issues',
               'labels': 'Bot'})

def get_documentation_issue():
    data = get('issues?labels=Bot')
    # Check if the right issue exists and get its id
    id = None
    out_data = None
    for d in data:
        if d['title'] == 'Documentation issues':
            id = d['iid']
            out_data = d
    # Create the issue
    if id == None:
        create_issue()
    return id, out_data

def get_todos_from_description(description):
    todos = {}
    if description:
        lines = description.splitlines(True)
        for l in lines:
            if ('- [ ] ' in l) or ('- [x] ' in l) or ('- [X] ' in l):
                matches = re.finditer(TODO_REGEXP, l)
                for matchNum, match in enumerate(matches, start=1):
                    if match.group(1) == ' ':
                        status = 'TODO'
                    else:
                        status = 'DONE'
                    todos[match.group(2)] = {'status': status, 'line':l}
        return todos
    return

def get_previous_todos():
    _, data = get_documentation_issue()
    return get_todos_from_description(data['description']), data

def process_logs():

    id, _ = get_documentation_issue()

    todos = {}
    with open(args.log_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            log.debug("Line: " + line)
            s = line.split(':')
            if len(s) == 5 and (' Entrée TODO trouvée ' in s or ' TODO entry found' in s):
                log.info("Found todo: " + line)
                file = os.path.relpath(s[0], args.root_path)
                file = file[:-4] + '.html'
                if file in todos:
                    todos[file].append(s[4][1:-1])
                else:
                    todos[file] = [s[4][1:-1]]

    # Now build string description
    description = "Here is an **automatically** generated issue from a bot.\n\n"
    description += "The bot has gathered the todo items from the documentation:\n"
    for file in todos:
        for i, todo in enumerate(todos[file]):
            description += '- [ ] ' + todo + ' ([here](' + os.path.join(args.website_url, file) + '#id' + str(i+1) + '))\n'

    todos = get_todos_from_description(description)
    old_todos, old_description = get_previous_todos()

    
    # Now compare new and old todos
    if old_todos:
        for ot in old_todos:
            if todos and ot not in todos:
                # it has been fixed now
                if old_todos[ot]['status'] == 'TODO':
                    description += '- [x] ' + ot + ' (#' + current_commit + ')\n'
                else: # was already fixed in previous commit
                    description += old_todos[ot]['line'] + '\n'

    log.info(description)

    if old_description != description:
        update_issue(id, data={'description': description})


if __name__ == '__main__':
    process_logs()
