base_url = 'https://gitlab.inria.fr/'
project_id = 15931
website_url = 'https://cityval.gitlabpages.inria.fr/supervisor/'
root_path = '/home/toullier/Documents/Code/projects/CityVal/supervisor/doc/source/'
log_path = 'sphinx-log.log'


# python3 bot.py https://gitlab.inria.fr/ 15931 https://cityval.gitlabpages.inria.fr/supervisor/ /home/toullier/Documents/Code/projects/CityVal/supervisor/doc/source/ sphinx-log.log

# python3 bot.py $CI_SERVER_PROTOCOL://$CI_SERVER_HOST/ $CI_PROJECT_ID $CI_PAGES_URL $CI_PROJECT_DIR/doc/source/ sphinx-log.log
