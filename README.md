
# Table of Contents

1.  [Introduction](#org2eb0e7a)
2.  [Installation](#orge06f268)
3.  [Creating a job](#org634bc3d)
    1.  [Hello World](#orge0d4339)
4.  [Documentation](#org5944dda)


<a id="org2eb0e7a"></a>

# Introduction

This project intends to batch process many files at ones. 
The idea is to use decorators to easily execute a particular function 
on many files.

Furthermore, one can use a `yaml` configuration file that can save
some configurations for different batch processes that uses the same functions.


<a id="orge06f268"></a>

# Installation

To install it, you can use `pip`:

``` bash
	pip install mrmeeseeks
```

Then, you can import it 
``` python
	from mr.meeseeks import Meeseeks
```

<a id="org634bc3d"></a>


# Documentation

Check out the full documentation at [https://kirmtwinty.gitlab.io/mrmeeseeks](https://kirmtwinty.gitlab.io/mrmeeseeks) for more details.

