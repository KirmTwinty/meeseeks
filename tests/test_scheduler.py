import unittest
from mr.utils import OhNo
from mr.moment import Moment
from mr.scheduler import action, scheduler


class TestActions(unittest.TestCase):
    """Test the action decorator.

    Make unit tests on action decorator.
    """

    def test_actions_definitions(self):
        @action
        def hello_world():
            return 'Hello world'

        self.assertTrue('hello_world' in scheduler.tasks_list)
        t = scheduler.get('hello_world')
        self.assertEqual(t.name, 'hello_world')
        self.assertEqual(t.action.__name__, 'hello_world')
        self.assertEqual(t.action(), 'Hello world')

    def test_actions_args(self):
        # Empty argument
        scheduler.reset()

        @action()
        def hello_world():
            return 'Hello world'

        t = scheduler.tasks_list['hello_world']
        self.assertEqual(t.action(), 'Hello world')

        # Many arguments
        scheduler.reset()
        import datetime as dt
        time = dt.datetime.now()

        kwargs = {
            'when' : Moment.BEFORE,
            'rate' : 2,
            'begin' : time,
            'end' : time + dt.timedelta(days=1),
            'depends' : 'other',
            'name' : 'custom_action'
        }

        @action(**kwargs)
        def hello_world_2(s):
            return s

        with self.assertRaises(KeyError):
            scheduler.tasks_list['hello_world_2']

        self.assertTrue('custom_action' in scheduler.tasks_list)
        t = scheduler.get('custom_action')
        self.assertEqual(t.action('my_string'), 'my_string')
        for k in kwargs:
            self.assertEqual(getattr(t, k), kwargs[k])

    def test_multiple_actions(self):
        scheduler.reset()

        @action(when=Moment.BEFORE)
        @action(when=Moment.ON_ITEM)
        @action(when=Moment.AFTER)
        def multiple_actions(s):
            return s

        self.assertEqual(len(scheduler.tasks_list), 3)
        self.assertEqual(getattr(scheduler.get('multiple_actions_2'), 'when'),
                         Moment.BEFORE)
        self.assertEqual(getattr(scheduler.get('multiple_actions_1'), 'when'),
                         Moment.ON_ITEM)
        self.assertEqual(getattr(scheduler.get('multiple_actions'), 'when'),
                         Moment.AFTER)
        self.assertEqual(scheduler.get('multiple_actions').action('a'), 'a')
        self.assertEqual(scheduler.get('multiple_actions_1').action('b'), 'b')
        self.assertEqual(scheduler.get('multiple_actions_2').action('c'), 'c')

class TestScheduler(unittest.TestCase):
    """Test the Scheduler class.

    Make unit tests on Scheduler class, particularly the
    dependencies.
    """

    def test_no_dependency(self):
        # No dependency
        scheduler.reset()
        funcnames = ['hello_world', 'hello_world_2']

        @action
        def hello_world():
            return 'Hello world'

        @action
        def hello_world_2():
            return 'Hello world'

        scheduler.schedule()

        self.assertTrue(scheduler.check_dependencies())

        names = [t.name for t in scheduler.batches]
        for fname in funcnames:
            self.assertTrue(fname in names)

    def test_simple_dependency(self):
        # simple dependency
        scheduler.reset()
        funcnames = ['hello_world', 'hello_world_1', 'hello_world_2']

        @action(depends=['hello_world_1'])
        def hello_world_2():
            return 'Hello world'

        @action
        def hello_world():
            return 'Hello world'

        @action(depends=['hello_world'])
        def hello_world_1():
            return 'Hello world'

        scheduler.schedule()

        for i, fname in enumerate(funcnames):
            self.assertEqual(fname, scheduler.batches[i].name)

    def test_simple_dependency_2(self):
        scheduler.reset()
        funcnames = ['hello_world', 'hello_world_1', 'hello_world_2']

        @action(depends=['hello_world_1'])
        def hello_world_2():
            return 'Hello world'

        @action(depends=['hello_world'])
        def hello_world_1():
            return 'Hello world'

        @action
        def hello_world():
            return 'Hello world'

        scheduler.schedule()

        for i, fname in enumerate(funcnames):
            self.assertEqual(fname, scheduler.batches[i].name)

    def test_itself_dependency(self):
        scheduler.reset()

        @action
        def works():
            return 'It would work.'

        @action(depends=['i_depend_on_me'])
        def i_depend_on_me():
            return 'It should fail.'

        with self.assertRaises(OhNo):
            scheduler.schedule()

    def test_circular_dependency(self):
        scheduler.reset()

        @action(depends=['depend_1'])
        def depend_0():
            return 'I depend on 1.'

        @action(depends=['depend_2'])
        def depend_1():
            return 'I depend on 2.'

        @action(depends=['depend_0'])
        def depend_2():
            return 'I depend on 0.'

        with self.assertRaises(OhNo):
            scheduler.schedule()

if __name__ == '__main__':
    unittest.main()
