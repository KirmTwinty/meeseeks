import unittest
from mr.meeseeks import Meeseeks
from mr.scheduler import action


class TestBatch(unittest.TestCase):
    """Test some batch functions.
    """

    def test_batch(self):
        @action(rate=2)
        def print_fname(filename):
            print(filename)

        Meeseeks(folders=['./**/*.py'])

if __name__ == '__main__':
    unittest.main()
